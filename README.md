# ParticleGUI
## Engelsk
###### Project made with C/C++/ROOT. The project is a GUI for a visualisation of a different version of the standardmodel. All information is saved in .root format. The project was made in relation to the subject PHYS291 at University of Bergen.
## Norsk
###### Prosjekt laget i C/C++/ROOT. Laget en GUI for en fremstilling av standardmodellen, hvor all informasjon er lagret i .root filer.  Prosjektet ble laget i forbindelse med faget PHYS291 og rapporten kan leses på siden under.
## [Particle GUI webpage](https://folk.uib.no/oas004/index291.html)
