//
// Author: Odin Asbjørnsen
// Email: oas004@student.uib.no	
// 
//
// Thanks to Ladislav Kocbach for the graphical design
//
// Code inspired by WoldMap.C by Valeriy Onuchin



#include <iostream>
#include <string>
#include <TGPicture.h>
#include <TGMenu.h>
#include <TGImageMap.h>
#include <TGMsgBox.h>
#include <TGClient.h>
//#include "TStyle.h"
//#include "TF2.h"
//#include "TTimer.h"
using namespace std;
	//This is the initial class that is needed for root to know where to start
	class project
	{
	public:
	project() { cout << "The project is starting";}
	
	
	};
	//-----------------------------------------------------------------------
	// This is the lepton class  that retriews information from the root file.
	class Lepton
	{
	public:
	Lepton() {  cout << " Lepton constructor... \n";}
	Lepton(string info):leptonInformation(info) {cout << "Lepton constructor... \n";}
	~Lepton() { cout << "Lepton destructor...\n";}
	Lepton (const Lepton & rhs);
	string getLeptonInfo() const {return leptonInformation;}
	virtual void Info() const { cout << "Particle information";}
	void GetLeptonInfoRoot() {

		TFile *fLep = new TFile("Information.root");
		//fLep->ls();
		
		TString * lepInfo;
		
		lepInfo = (TString*) fLep->Get("LeptonInfoRoot");

		cout << *lepInfo << endl;
	}
	protected:
	const string leptonInformation;
	};
	Lepton::Lepton (const Lepton & rhs):leptonInformation(rhs.getLeptonInfo())
	{	
		cout << "copy constructor";
	} 
	//---------------------------------------------------------------------------


	//This is the neutrino class. This class inherits the lepton class. It reads info from the root file
	class Neutrino : public Lepton
	{
	public:
		Neutrino() { cout << "Neutrino constructor\n";}
		~Neutrino() { cout << "Neutrino deconstructor \n";}
		Neutrino (const Neutrino & rhs);
		void GetNeutrinoInfoRoot() {
			TFile *fNeu = new TFile("Information.root");
			//fNeu->ls();
			TString *neutrinoInf;
			neutrinoInf = (TString*) fNeu->Get("NeutrinoInfoRoot");
			cout << *neutrinoInf << endl; 
		}
		virtual Lepton* Clone() {return new Lepton(*this);}

	};
	Neutrino ::Neutrino (const Neutrino & rhs):Lepton(rhs)
	{
		cout << "Neutrino copy constructor...\n";
	}
	//--------------------------------------------------------------------------------------------------
	
 	//This is the Tau class with inheritance from the Lepton class. It reads from the root file
	class Tau : public Lepton
        {
        public:
                Tau() { cout << "Tau constructor\n";}
                ~Tau() { cout << "Tau deconstructor \n";}
                Tau (const Tau & rhs);
                string getTauInformation() const{return TauInfo;}
                virtual Lepton* Clone() {return new Lepton(*this);}
		void GetTauInfoRoot() {
                        TFile *fTau = new TFile("Information.root");
                        //fTau->ls();
                        TString *tauInf;
                        tauInf = (TString*) fTau->Get("TauInfoRoot");
                        cout << *tauInf << endl; 
                }
                protected:
                const string TauInfo; 
        };
        Tau ::Tau (const Tau & rhs):Lepton(rhs)
        {
                cout << "Tau copy constructor...\n";
        }
	//-------------------------------------------------------------------------------------------------


	// This is the muon class with inheritance from the Lepton class. It reads from the  root file .
	 class Muon : public Lepton
        {
        public:
                Muon() { cout << "Muon constructor\n";}
                ~Muon() { cout << "Muon deconstructor \n";}
                Muon (const Muon & rhs);
                string getMuonInformation() const{return MuonInfo;}
                virtual Lepton* Clone() {return new Lepton(*this);}
		void GetMuonInfoRoot() {
                        TFile *fMu = new TFile("Information.root");
                        //fMu->ls();
                        TString *muInf;
                        muInf = (TString*) fMu->Get("MuonInfoRoot");
                        cout << *muInf << endl; 
                }
                protected:
                const string MuonInfo; 
        };
        Muon::Muon (const Muon & rhs):Lepton(rhs)
        {
                cout << "Muon copy constructor...\n";
        }
	//---------------------------------------------------------------------------------------------------

	//This is the electron class, it has inheritance from lepton and also reads from the root file at electrons location
 	class Electron : public Lepton
        {
        public:
                Electron() { cout << "Electron constructor\n";}
                ~Electron() { cout << "Electron deconstructor \n";}
                Electron (const Electron & rhs);
                string getElectronInformation() const{return ElectronInfo;}
                virtual Lepton* Clone() {return new Lepton(*this);}
		void GetElectronInfoRoot() {
                        TFile *fEl = new TFile("Information.root");
                        //fEl->ls();
                        TString *elInf;
                        elInf = (TString*) fEl->Get("ElectronInfoRoot");
                        cout << *elInf << endl; 
                }
                protected:
                const string ElectronInfo; 
        };
        Electron::Electron (const Electron & rhs):Lepton(rhs)
        {
                cout << "Electron copy constructor...\n";
        }
	//------------------------------------------------------------------------------------------------------------------

	//This is the boson class, it reads from the root file to retrieve information about the boson
        class Boson
        {
        public:
        Boson() {  cout << " Bosonn constructor... \n";}
        Boson(string info):bosonInformation(info) {cout << "Boson constructor... \n";}
        ~Boson() { cout << "Boson destructor...\n";}
        Boson (const Boson & rhs);
        virtual void Info() const { cout << "Particle information";} 
        string getBosonInfo() const {return bosonInformation;}
	void GetBosonInfoRoot() {
                        TFile *fBo = new TFile("Information.root");
                        //fBo->ls();
                        TString *boInf;
                        boInf = (TString*) fBo->Get("BosonInfoRoot");
                        cout << *boInf << endl; 
                }
        protected:
        const string bosonInformation;
        }; 
         
        Boson::Boson (const Boson & rhs):bosonInformation(rhs.getBosonInfo())
        {
                cout << "Boson Copy Constructor....\n";
        }
	//------------------------------------------------------------------------------------------------------------------



	//This is the class for the Neutral weak boson, that inherits the boson class and reads from the root file.
	class Neutral_Weak : public Boson
        {
        public:
                Neutral_Weak() { cout << "Neutral_Weak constructor\n";}
                ~Neutral_Weak() { cout << "Neutral_Weak deconstructor \n";}
                Neutral_Weak (const Neutral_Weak & rhs);
                string getNeutral_WeakInformation() const {return neutral_WeakInfo;} 
                virtual Boson* Clone() {return new Boson(*this);}
		void GetNaturalWeakInfoRoot() {
                        TFile *fNW = new TFile("Information.root");
                        //fNW->ls();
                        TString *nwInf;
                        nwInf = (TString*) fNW->Get("NaturalWeakInfoRoot");
                        cout << *nwInf << endl; 
                }
                protected: 
                const string neutral_WeakInfo; 
        };
        Neutral_Weak ::Neutral_Weak (const Neutral_Weak & rhs):Boson(rhs)
        {
                cout << "Neutral_Weak copy constructor...\n";
        }
	//----------------------------------------------------------------------------------------------------------------


	//This is the weak charged boson class that has inheritance from boson. Furthermore, it reads from the root file at the location
	class Charged_Weak : public Boson
        {
        public:
                Charged_Weak() { cout << "Charged weak boson constructor\n";}
                ~Charged_Weak() { cout << "Charged weak boson deconstructor \n";}
                Charged_Weak (const Charged_Weak & rhs);
                string getCharged_WeakInformation() const {return Charged_WeakInfo;}
                virtual Boson* Clone() {return new Boson(*this);}
		void GetChargedWeakInfoRoot() {
                        TFile *fCW = new TFile("Information.root");
                        //fCW->ls();
                        TString *cwInf;
                        cwInf = (TString*) fCW->Get("ChargedWeakInfoRoot");
                        cout << *cwInf << endl; 
                }
                protected: 
                const string Charged_WeakInfo; 
        };
        Charged_Weak ::Charged_Weak (const Charged_Weak & rhs):Boson(rhs)
        {
                cout << "Weak charged boson copy constructor...\n";
        }
	//------------------------------------------------------------------------------------------------------------------------------

	//This is the gluon class that has inheritance from boson. It also reads from the root file at the specific location.
	class Gluon : public Boson
        {
        public:
                Gluon() { cout << "Gluon constructor\n";}
                ~Gluon() { cout << "Gluon deconstructor \n";}
                Gluon (const Gluon & rhs);
                string getGluonInformation() const {return GluonInfo;} 
                virtual Boson* Clone() {return new Boson(*this);}
		void GetGluonInfoRoot() {
                        TFile *fGl = new TFile("Information.root");
                        //fGl->ls();
                        TString *glInf;
                        glInf = (TString*) fGl->Get("GluonInfoRoot");
                        cout << *glInf << endl; 
                }

                protected: 
                const string GluonInfo; 
        };
        Gluon::Gluon (const Gluon & rhs):Boson(rhs)
        {
                cout << "Gluon copy constructor...\n";
        }
	//-------------------------------------------------------------------------------------------------------------------------------


	//This is the foton class that has inheritance from boson. It also reads from the root file at the foton location
	class Foton : public Boson
        {
        public:
                Foton() { cout << "Foton constructor\n";}
                ~Foton() { cout << "Foton deconstructor \n";}
                Foton (const Foton & rhs);
                string getFotonInformation() const {return FotonInfo;} 
                virtual Boson* Clone() {return new Boson(*this);}
		void GetFotonInfoRoot() {
                        TFile *fFo = new TFile("Information.root");
                        //fFo->ls();
                        TString *foInf;
                        foInf = (TString*) fFo->Get("FotonInfoRoot");
                        cout << *foInf << endl; 
                }
                protected: 
                const string FotonInfo; 
        };
        Foton::Foton(const Foton & rhs):Boson(rhs)
        {
                cout << "Foton copy constructor...\n";
        }
	//------------------------------------------------------------------------------------------------------------------------------


	// This is the higgs boson class that has inheritance from boson. Further it reads from the root file at the higgs location
	class Higgs : public Boson
        {
        public:
                Higgs() { cout << "Higgs constructor\n";}
                ~Higgs() { cout << "Higgs deconstructor \n";}
                Higgs (const Higgs & rhs);
                string getHiggsInformation() const {return HiggsInfo;} 
                virtual Boson* Clone() {return new Boson(*this);}
		void GetHiggsInfoRoot() {
                        TFile *fHg = new TFile("Information.root");
                        //fHg->ls();
                        TString *hgInf;
                        hgInf = (TString*) fHg->Get("HiggsInfoRoot");
                        cout << *hgInf << endl; 
                }
                protected: 
                const string HiggsInfo; 
        };
        Higgs::Higgs (const Higgs & rhs):Boson(rhs)
        {
                cout << "Higgs copy constructor...\n";
        }
	//----------------------------------------------------------------------------------------------------------------------------

	// This is the Quark class family class. It also reads from the quark location in the root file.
       class Quark
        {
        public:
        Quark() {  cout << " Quark constructor... \n";}
        Quark(string info):quarkInformation(info) {cout << "Quark constructor... \n";}
        ~Quark() { cout << "Quark destructor...\n";}
        Quark (const Quark & rhs);
        virtual void Info() const { cout << "Particle information";} 
        string getQuarkInfo() const {return quarkInformation;}
	void GetQuarkInfoRoot() {
                        TFile *fQ = new TFile("Information.root");
                        //fQ->ls();
                        TString *quInf;
                        quInf = (TString*) fQ->Get("QuarkInfoRoot");
                        cout << *quInf << endl; 
                }
        protected:
        const string quarkInformation;
        }; 
         
        Quark::Quark (const Quark & rhs):quarkInformation(rhs.getQuarkInfo()) 
        {
                cout << "Quark Copy Constructor....\n";
        }
	//---------------------------------------------------------------------------------------------

	//This is the top quark class that has inheritance from  the quark class. It also reads from the top quark location in the root file.
	class Top : public Quark
        {
        public:
                Top() { cout << "Top constructor\n";}
                ~Top() { cout << "Top deconstructor \n";}
                Top (const Top & rhs);
                string getTopInformation() const{return TopInfo;} 
                virtual Quark* Clone() {return new Quark(*this);}
		void GetTopInfoRoot() {
                        TFile *fT = new TFile("Information.root");
                        //fT->ls();
                        TString *tInf;
                        tInf = (TString*) fT->Get("TopInfoRoot");
                        cout << *tInf << endl; 
                }
                protected: 
                const string TopInfo; 
        };
        Top ::Top (const Top & rhs):Quark(rhs)
        {
                cout << "Top copy constructor...\n";
        }
	//-------------------------------------------------------------------------------------------------------------------------------------	



	//This  is the bottom quark class that has inheritance from the quark class. It also reads from the bottom quark location in the root file.
	class Bottom : public Quark
        {
        public:
                Bottom() { cout << "Bottom constructor\n";}
                ~Bottom() { cout << "Bottom deconstructor \n";}
                Bottom (const Bottom & rhs);
                string getBottomInformation() const {return BottomInfo;} 
                virtual Quark* Clone() {return new Quark(*this);}
		void GetBottomInfoRoot() {
                        TFile *fB = new TFile("Information.root");
                        //fB->ls();
                        TString *bInf;
                        bInf = (TString*) fB->Get("BottomInfoRoot");
                        cout << *bInf << endl; 
                }
                protected: 
                const string BottomInfo; 
        };
        Bottom ::Bottom (const Bottom & rhs):Quark(rhs)
        {
                cout << "Bottom copy constructor...\n";
        }
	//------------------------------------------------------------------------------------------------------------------------------------------

	// This is the charm quark class that has inheritance from quark class. Further it reads from the charm location in the root file.
	class Charm : public Quark
        {
        public:
                Charm() { cout << "Charm constructor\n";}
                ~Charm() { cout << "Charm deconstructor \n";}
                Charm (const Charm & rhs);
                string getCharmInformation()const {return CharmInfo;}
                virtual Quark* Clone() {return new Quark(*this);}
		void GetCharmInfoRoot() {
                        TFile *fCh = new TFile("Information.root");
                        //fCh->ls();
                        TString *chInf;
                        chInf = (TString*) fCh->Get("CharmInfoRoot");
                        cout << *chInf << endl; 
                }
                protected: 
                const string CharmInfo; 
        };
        Charm ::Charm (const Charm & rhs):Quark(rhs)
        {
                cout << "Charm copy constructor...\n";
        }
	//------------------------------------------------------------------------------------------------------------------------------

	// This is the strange quark class that has inheritance from the quark class. It also reads from the strange location in the root file.
	class Strange : public Quark
        {
        public:
                Strange() { cout << "Strange constructor\n";}
                ~Strange() { cout << "Strange deconstructor \n";}
                Strange (const Strange & rhs);
                string getStrangeInformation()const{return StrangeInfo;} 
                virtual Quark* Clone() {return new Quark(*this);}
		void GetStrangeInfoRoot() {
                        TFile *fSt = new TFile("Information.root");
                        //fSt->ls();
                        TString *stInf;
                        stInf = (TString*) fSt->Get("StrangeInfoRoot");
                        cout << *stInf << endl; 
                }
                protected: 
                const string StrangeInfo; 
        };
        Strange ::Strange (const Strange & rhs):Quark(rhs)
        {
                cout << "Strange copy constructor...\n";
        }
	//------------------------------------------------------------------------------------------------------------------------------------------


	// This is the up quark class that has inheritance from the quark class. Also it reads from the up location in the root file.
	class Up : public Quark
        {
        public:
                Up() { cout << "Up constructor\n";}
                ~Up() { cout << "Up deconstructor \n";}
                Up (const Up & rhs);
                string getUpInformation() const{return UpInfo;} 
                virtual Quark* Clone() {return new Quark(*this);}
		void GetUpInfoRoot() {
                        TFile *fUp = new TFile("Information.root");
                        //fUp->ls();
                        TString *upInf;
                        upInf = (TString*) fUp->Get("UpInfoRoot");
                        cout << *upInf << endl; 
                }
                protected: 
                const string UpInfo; 
        };
        Up::Up (const Up & rhs):Quark(rhs)
        {
                cout << "Up quark copy constructor...\n";
        }
	//--------------------------------------------------------------------------------------------------------------------------------

	// This is the down quark class that has inheritance from the quark class. Further it also reads from the down position in the root file.
	class Down : public Quark
        {
        public:
                Down() { cout << "Down constructor\n";}
                ~Down() { cout << "Down deconstructor \n";}
                Down (const Down & rhs);
                string getDownInformation() const{return DownInfo;} 
                virtual Quark* Clone() {return new Quark(*this);}
		void GetDownInfoRoot() {
                        TFile *fD = new TFile("Information.root");
                      //  fD->ls();
                        TString *dInf;
                        dInf = (TString*) fD->Get("DownInfoRoot");
                        cout << *dInf << endl; 
                }
                protected: 
                const string DownInfo; 
        };
        Down ::Down (const Down & rhs):Quark(rhs)
        {
                cout << "Down copy constructor...\n";
        }
	//------------------------------------------------------------------------------------------------------------------------------


	// This is the class that is handling the  GUI and connecting all the other classes together.
	class std_modell
	{
	protected:

		TGMainFrame* fMain; //main frame
		TGImageMap* fImageMap; // image map

		//function initialising. 
		virtual void InitMap();
		virtual void InitLepton(); //code 1
		virtual void InitNeutrino(); //code 2
		virtual void InitTau(); //code 3
		virtual void InitMuon(); //code 4
		virtual void InitElectron(); //code 5
		virtual void InitBoson(); //code 6
		virtual void InitNaturalWeak(); //code 7
		virtual void InitChargedWeak(); // code 8
		virtual void InitGluon(); // code 9
		virtual void InitFoton(); // code 10
		virtual void InitHiggs(); // code 11
		virtual void InitQuark(); // code 12
		virtual void InitTop(); // code 13
		virtual void InitBottom(); // code 14
		virtual void InitCharm(); // code 15
		virtual void InitStrange(); // code 16
		virtual void InitUp(); // code 17
		virtual void InitDown(); // code 18
	public:
		//This is a clasification of the different particles. Every particle gets a code that is called upon when the user clicks on a particle.
		enum particleCode {
			kLep = 1, kNeu = 2, kTau = 3, kMu = 4, kEl = 5, kB = 6, kNW = 7, kCW = 8, kGl = 9, kF = 10, kH = 11, kQ = 12, kT=13, kBo = 14, kS=15, kC=16, kU=17, kD=18 
		};
		std_modell(const char *picName = "std_mod.png"); // Getting the pictures in the frame
		virtual ~std_modell() {}
		
		virtual void Show() { fMain->MapRaised(); }
		TGImageMap* GetImageMap() const {return fImageMap;}
		virtual TString GetTitle() const;

		void PrintInfo(int info); // Raising different messageboxes from different codes.
	};

	std_modell::std_modell(const char* picName)
	{
		fMain = new TGMainFrame(gClient->GetRoot(), 750, 420); // defining the mainframe

		fImageMap = new TGImageMap(fMain, picName);
		fMain->AddFrame(fImageMap);
		fMain->SetWindowName(GetTitle().Data());
		fMain->SetIconName("Den Ustandariserte StandardModellen");

		TGDimension size = fMain->GetDefaultSize();
		fMain->Resize(size);
		fMain->MapSubwindows();
		InitMap();

		fImageMap->Connect("RegionClicked(int)", "std_modell", this, "PrintInfo(int)"); //This is used for connecting the different regions defined in the initfunctions to the numbercode.

	}
	TString std_modell::GetTitle() const
	{
		//Returns the title 
		return "Den ustandariserte standardmodellen, rightclick for popupmenu ";
	}

	//This is a function in std_modell class that initializes the lepton lables and regions
	void std_modell::InitLepton()
	{
		Lepton *l = new Lepton;

 		int x[12] = { 11, 91, 100, 7 };
   		int y[12] = { 198, 190, 315, 351 };
		
		
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kLep);
		fImageMap->SetToolTipText(kLep, "Lepton");
		

		TGPopupMenu *pm = fImageMap->CreatePopup(kLep);
		pm->AddLabel("Leptons");
		pm->AddSeparator();
		pm->AddEntry("Nautrinos", 812);
		pm->AddEntry("Tau particles", 967);
		pm->AddEntry("Muon particles", 812);
		pm->AddEntry("Electrons", 517);
		cout << "Lepton Popupmenu was added";

		
	}
	//This function initializes the nautrino lables and regions
	void std_modell::InitNeutrino()
	{
		Neutrino *n = new Neutrino;
		n->GetNeutrinoInfoRoot();
		int x[5] = { 74, 35, 222, 222, 395 };
   		int y[5] = { 324, 460, 406, 493, 518 };

		TGRegion reg(5, x, y);
		fImageMap->AddRegion(reg, kNeu);
		fImageMap->SetToolTipText(kNeu, "Neutrino"); 
		cout << "Neutrino";
	}
	//This function initializes Tau lables and regions
	void std_modell::InitTau()
	{

		Tau *t = new Tau;
		t->GetTauInfoRoot();
		int x[4] = {124, 192, 285, 233 };
		int y[4] = {337, 390, 289, 221 };
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kTau);
		fImageMap->SetToolTipText(kTau, "Tau Particle");
	}
	//This function inits Muon lables and regions
	void std_modell::InitMuon()
	{
		Muon *m = new Muon;
		m->GetMuonInfoRoot();
		int x[4] = {204, 287, 344, 288};
		int y[4] = {387, 447, 378, 299};
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kMu);
		fImageMap->SetToolTipText(kMu, "Muon Particle");
	}
        //This function inits electron lables and regions
	void std_modell::InitElectron()
	{
		Electron *e = new Electron;
		e->GetElectronInfoRoot();
		int x[4] = {288, 398, 423, 356};
		int y[4] = {446, 508, 491, 384};
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kEl);
		fImageMap->SetToolTipText(kEl, "Electron");
	}
	//This function inits The bosonic lables and regions
	void std_modell::InitBoson()
	{
		Boson *b = new Boson;
		b->GetBosonInfoRoot();
		int x[4] = {349, 351, 515, 521};
		int y[4] = {9, 61, 56, 11};
		TGRegion reg(4,x, y);
		fImageMap->AddRegion(reg, kB);
		fImageMap->SetToolTipText(kB, "Boson");

		TGPopupMenu *pm = fImageMap->CreatePopup(kB);
                pm->AddLabel("Bosons");
                pm->AddSeparator();
                pm->AddEntry("Natural weak boson", 111);
                pm->AddEntry("Charged Weak boson", 222);
                pm->AddEntry("Gluon boson", 333);
                pm->AddEntry("Foton boson", 444);
		pm->AddEntry("Higgs boson", 555);
                cout << "Boson Popupmenu was added";
	}
	//This function inits the Natural weak boson lables and regions
	void std_modell::InitNaturalWeak()
        {
                Neutral_Weak *nw = new Neutral_Weak;
                nw->GetNaturalWeakInfoRoot();
                int x[4] = {245, 386, 402, 291};
                int y[4] = {220, 168, 255, 288};
                TGRegion reg(4, x, y);
                fImageMap->AddRegion(reg, kNW);
                fImageMap->SetToolTipText(kNW, "Neutral Weak Boson");
        }
	//This function inits the Charged Weak boson lables and regions
	void std_modell::InitChargedWeak()
	{
		Charged_Weak *cw = new Charged_Weak;
		cw->GetChargedWeakInfoRoot();
		int x[6] = {386, 524, 651, 612, 553, 469};
                int y[6] = {168, 176, 233, 281, 276, 329};
		TGRegion reg(6, x, y);
		fImageMap->AddRegion(reg, kCW);
		fImageMap->SetToolTipText(kCW, "Charged weak boson");
	}
	//This function inits the Gluon boson lables and regions
	void std_modell::InitGluon()
	{
		Gluon *g = new Gluon;
		g->GetGluonInfoRoot();
		int x[4] = {457, 488, 611, 568};
                int y[4] = {450, 481, 301, 279};
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kGl);
		fImageMap->SetToolTipText(kGl, "Gluon");
	}
	//This function inits the Foton boson lables and regions
	void std_modell::InitFoton()
	{
		Foton *f = new Foton;
		f->GetFotonInfoRoot();
		int x[4] = {305, 425, 463, 367};
                int y[4] = {305, 480, 458, 282};
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kF);
		fImageMap->SetToolTipText(kF, "Foton");
	}
	//This function inits the Higgs boson lables and regions
	void std_modell::InitHiggs()
	{
		Higgs *h = new Higgs;
		h->GetHiggsInfoRoot();
		int x[6] = {193, 208, 436, 441, 706, 680};
                int y[6] = {205, 236, 145, 64, 208, 228};
		TGRegion reg(6, x, y);
		fImageMap->AddRegion(reg, kH);
		fImageMap->SetToolTipText(kH, "Higgs boson");
	}
	//This function inits the quark lables and regions
	void std_modell::InitQuark()
	{
		Quark *q = new Quark;
		q->GetQuarkInfoRoot();
		int x[6] = {700, 782, 881, 889};
                int y[6] = {230, 174, 426, 449};
		TGRegion reg(6, x, y);
		fImageMap->AddRegion(reg, kQ);
		fImageMap->SetToolTipText(kQ, "Quark particle");

		TGPopupMenu *pmQ = fImageMap->CreatePopup(kQ);
                pmQ->AddLabel("Quarks");
                pmQ->AddSeparator();
                pmQ->AddEntry("Top Quark", 666);
                pmQ->AddEntry("Bottom Quark", 777);
                pmQ->AddEntry("Charm Quark", 888);
                pmQ->AddEntry("Strange Quark", 999);
                pmQ->AddEntry("Up Quark", 100);
		pmQ->AddEntry("Down Quark", 101);
                cout << "Quark Popupmenu was added";

	}
	//This function inits the Top Quark lable and regions
	void std_modell::InitTop()
	{
		Top *t = new Top;
		t->GetTopInfoRoot();
		int x[4] = {674, 611, 692, 783};
                int y[4] = {235, 313, 416, 373};
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kT);
		fImageMap->SetToolTipText(kT, "Top Quark");
                
	}
	//This function inits the Bottom Quark lable and regions
	void std_modell::InitBottom()
	{
		Bottom *bo = new Bottom;
		bo->GetBottomInfoRoot();
		int x[4] = {781, 699, 720, 816};
                int y[4] = {381, 421, 504, 498};
		TGRegion reg(4,x, y);
		fImageMap->AddRegion(reg,kBo);
		fImageMap->SetToolTipText(kBo, "Bottom Quark");
	}
	//This function inits the Charm Quark lable and regions
	void std_modell::InitCharm()
	{
		Charm *c = new Charm;
		c->GetCharmInfoRoot();
		int x[4] = {606, 560, 609, 687};
                int y[4] = {318, 391, 456, 417};
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kC);
		fImageMap->SetToolTipText(kC, "Charm Quark");
	}
	//This function inits the Strange Quark lable and regions
	void std_modell::InitStrange()
	{
		Strange *s = new Strange;
		s->GetStrangeInfoRoot();
		int x[4] = {688, 611, 629, 713};
                int y[4] = {428, 464, 515, 503};
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kS);
		fImageMap->SetToolTipText(kS, "Strange Quark");
	}
	//This function inits the Up Quark and regions
	void std_modell::InitUp()
	{
		Up *u = new Up;
		u->GetUpInfoRoot();
		int x[4] = {554, 487, 508, 602};
                int y[4] = {403, 487, 511, 459};
		TGRegion reg(4, x, y);
		fImageMap->AddRegion(reg, kU);
		fImageMap->SetToolTipText(kU, "Up Quark");
	}
	//This function inits the Down Quark and regions
	void std_modell::InitDown()
	{
		Down *d = new Down;
		d->GetDownInfoRoot();
		int x[4] = {598, 500, 508, 622};
                int y[4] = {467, 510, 530, 513};
		TGRegion reg(4,x,y);
		fImageMap->AddRegion(reg, kD);
		fImageMap->SetToolTipText(kD, "Down Quark");
	}
	void std_modell::InitMap()
	{
		//This is where the functions are called upon
		InitLepton();
		InitNeutrino();
		InitTau();
		InitMuon();
		InitElectron();
		InitBoson();
		InitNaturalWeak();
                InitChargedWeak();
		InitGluon();
		InitFoton();
		InitHiggs();
		InitQuark();
		InitTop();
		InitBottom();
		InitCharm();
		InitStrange();
		InitUp();
		InitDown();
		fImageMap->SetToolTipText(GetTitle().Data(), 300);
	}
	void std_modell::PrintInfo(int info)
	{
		EMsgBoxIcon icontype = kMBIconAsterisk;
		int buttons = 0;
		int retval;
/*
		//Declaration variables
		Lepton *lep = new Lepton;
		Neutrino *n = new Neutrino;
		Tau *t = new Tau;
		Electron *e = new Electron;
		Boson *b = new Boson;
		Neutral_Weak *nw = new Neutral_Weak;
		Charged_Weak *cw = new Charged_Weak;
		Gluon *g = new Gluon;
		Foton *f = new Foton;
		Higgs *h = new Higgs;
		Quark *q = new Quark;
		Top * t = new Top;
		Bottom *b = new Bottom;
		Charm *c = new Charm;
		Strange *s = new Strange;
		Up *u = new Up;
		Down *d = new Down;
		//-----------------
*/


/*
	Here I tried to implement a way for the information that is stored in 
	the root file, to be shown as a seperate window. This caused a lot of
	crashes, however this could be some of the future for this GUI..
*/
		// If declarations that uses the codes assigned to each particle to determine what information that should be yielded.
		if(info == 1) 
		{
		Lepton *l; l->GetLeptonInfoRoot();
		new TGMsgBox(gClient->GetRoot(), fMain, "Leptones", Form("Leptones is a class of particles \n right click for class info", info), icontype, buttons, &retval);
		}

                if(info == 2) 
                {
                	Neutrino *n; n->GetNeutrinoInfoRoot();
                	new TGMsgBox(gClient->GetRoot(), fMain, "Neutrinos ", Form("Neutrinos \n is a class of particles", info), icontype, buttons, &retval);
		}

		if(info == 3) 
                {
                	Tau *t; t->GetTauInfoRoot();
                	new TGMsgBox(gClient->GetRoot(), fMain, "Tau particle", Form("Tau particle \n they are a member of the leptones", info), icontype, buttons, &retval);
                }
                
		if(info == 4) 
                {
                        Muon *m; m->GetMuonInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Muon particle", Form("Muon particle \n they are a member of the leptones", info), icontype, buttons, &retval);
                }

		if(info == 5) 
                {
                        Electron *e; e->GetElectronInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Electrons", Form("Electrons \n they are a member of the leptones", info), icontype, buttons,&retval);
                }

		if(info == 6) 
                {
                        Boson *b; b->GetBosonInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Bosons", Form("Bosons \n they are a class of particles, right click for class info", info), icontype, buttons,&retval);
                }

		if(info == 7) 
                {
                        Neutral_Weak *nw; nw->GetNaturalWeakInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Neutral weak boson", Form("Neutral weak \n they are a member of the boson family", info), icontype, buttons,&retval);
                }

		if(info == 8) 
                {
                        Charged_Weak *cw; cw->GetChargedWeakInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Charged Weak boson ", Form("Charged Weak \n they are a member of the boson family", info), icontype, buttons,&retval);
                }
		
		if(info == 9) 
                {
                        Gluon *g; g->GetGluonInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Gluon particle", Form("Gluon particle \n they are a member of the boson family", info), icontype, buttons,&retval);
                }
		
		if(info == 10) 
                {
                        Foton *f; f->GetFotonInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Photon particle", Form("Fotons \n they are a member of the boson family \n", info), icontype, buttons,&retval);
                }
		
		if(info == 11) 
                {
                        Higgs *h; h->GetHiggsInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Higgs particle ", Form("Higgs particle\n it is a member of the boson and is somethimes reffered to as the 'mystery particle'\n", info), icontype, buttons,&retval);
                }
		
		if(info == 12) 
                {
                        Quark *q; q->GetQuarkInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Quarks", Form("Quarks \n they are a class of particles. Right click for class info \n", info), icontype, buttons,&retval);
                }

		if(info == 13) 
                {
                        Top *t; t->GetTopInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Top Quark", Form("Top particle \n they are a member of the quarks \n", info), icontype, buttons,&retval);
                }

		if(info == 14) 
                {
                        Bottom *b; b->GetBottomInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Bottom Quark", Form("Bottom particle \n they are a member of the quarks \n", info), icontype, buttons,&retval);
                }
		
		if(info == 15) 
                {
                        Strange *s; s->GetStrangeInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Strange Quark", Form("Strange particle \n they are a member of the quarks", info), icontype, buttons,&retval);
                }
		
		if(info == 16) 
                {
                        Charm *c; c->GetCharmInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Charm Quark", Form("Charm particle \n they are a member of the quarks", info), icontype, buttons,&retval);
                }

		if(info == 17) 
                {
                        Up *u; u->GetUpInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Up Quark", Form("Up particle \n they are a member of the quarks", info), icontype, buttons,&retval);
                }

		if(info == 18) 
                {
                        Down *d; d->GetDownInfoRoot();
                        new TGMsgBox(gClient->GetRoot(), fMain, "Down Quark", Form("Down particle, they are a member of the quarks", info), icontype, buttons,&retval);
                }


		//------------------------------------------------------------------------------------------------------------------------------------------------------
	}



//--------------------End of classes-----------------------------------------------------------End of Classes----------------------------------------------------------------------------

// This is the main class section where the program starts

		//This is needed for root to know where to start
		#if defined(__CLING__)
	void  project()
		#else
	void main()

		#endif
	{
		for (int i = 0; i<1; i++)
		{
			cout << "The main function is starting...\n";
		}
		std_modell *map = new std_modell;
		map->Show();
		//map->PrintInfo(44);
	



}
